import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    SilicaFlickable {
        id: flickable
        anchors.fill: page
        contentHeight: column.height
        Column {
            id: column
            width: parent.width - Theme.paddingLarge*2
            spacing: 20
            anchors {
                left: parent.left
                leftMargin: Theme.paddingLarge
                //top: page.top
                //topMargin: Theme.paddingLarge
            }
            PageHeader { title: qsTr("AuroTris") }

            Label {
                width: parent.width
                text: qsTr("Classic tetris game, in AuroraOS style!")
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeMedium
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WordWrap
            }
            Label {
                width: parent.width
                text: qsTr("To play select 'New Game' in pulley menu!\nSwipe Left, Right or Down to move the block, click on the screen to Rotate it.\nSwipe Up to Pause the Game, to resume Swipe Up to show the Pulley Menu. Double Swipe Down to instant drop")
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignJustify
                wrapMode: Text.WordWrap
            }

            Label {
                width: parent.width
                text: qsTr("You gain 1 point for each step in the game and 10 points for each completed line! Multiple lines combo gives you 100 bonus points for each line completed! And 1000 points when four lines are cleared!!")
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignJustify
                wrapMode: Text.WordWrap
            }
            Label {
                width: parent.width
                text: qsTr("Do you prefer to play fast, more than strategically? If you can fill more lines consecutively you gain special Bonuses!\nIn each case every 1000 point you proceed to the next level, and the speed is increased!")
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignJustify
                wrapMode: Text.WordWrap
            }
        }

        VerticalScrollDecorator { flickable: flickable }

    }

}
