<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="20"/>
        <source>AuroTris</source>
        <translation>Авротрис</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="24"/>
        <source>Classic tetris game, in AuroraOS style!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="32"/>
        <source>To play select &apos;New Game&apos; in pulley menu!
Swipe Left, Right or Down to move the block, click on the screen to Rotate it.
Swipe Up to Pause the Game, to resume Swipe Up to show the Pulley Menu. Double Swipe Down to instant drop</source>
        <translation>Для начала игры выберите &quot;Новая игра&quot; в меню!
Свайп влево, вправо или вниз двигает блоки, нажатие на экран поворачивает их.
Свайв вверх ставит игру на паузу, еще один свайп покажет меню. Двойной свайп вниз для мгновенного падения фигурки</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="41"/>
        <source>You gain 1 point for each step in the game and 10 points for each completed line! Multiple lines combo gives you 100 bonus points for each line completed! And 1000 points when four lines are cleared!!</source>
        <translation>Вы получаете 1 очко за каждый шаг в игре и 10 очков за каждую завершенную линию! Комбинация из нескольких линий дает вам 100 бонусных очков за каждую завершенную линию! И 1000 очков, когда четыре линии будут очищены!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="49"/>
        <source>Do you prefer to play fast, more than strategically? If you can fill more lines consecutively you gain special Bonuses!
In each case every 1000 point you proceed to the next level, and the speed is increased!</source>
        <translation>Вы предпочитаете играть быстро, а не стратегически? Если вы сможете заполнить больше строк подряд, вы получите специальные бонусы!
В каждом случае каждые 1000 очков вы переходите на следующий уровень, и скорость увеличивается!</translation>
    </message>
</context>
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>About Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>#descriptionText</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="73"/>
        <source>3-Clause BSD License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="83"/>
        <source>#licenseText</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="38"/>
        <source>AuroTris
by Danyok</source>
        <translation>АвроТрис
by Danyok</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Very Hard</source>
        <translation>Очень тяжелый</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Hard</source>
        <translation>Тяжелый</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Normal</source>
        <translation>Нормальный</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Easy</source>
        <translation>Легкий</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Very Easy</source>
        <translation>Очень легкий</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="122"/>
        <source>Resume</source>
        <translation>Продолжить</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="127"/>
        <source>Save Game</source>
        <translation>Сохранить игру</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="136"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="141"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="154"/>
        <source>New Game</source>
        <translation>Новая игра</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="161"/>
        <source>Load Game</source>
        <translation>Загрузить игру</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="161"/>
        <source>No Game to Load</source>
        <translation>Нет вариантов для загрузки</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="316"/>
        <source>Combo x </source>
        <translation>Комбо x </translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="327"/>
        <source>Lost Combo</source>
        <translation>Комбо потеряно</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="338"/>
        <source>Gravity Bonus</source>
        <translation>Бонус гравитации</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="168"/>
        <source>Level </source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="168"/>
        <source>Score </source>
        <translation>Счёт</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="168"/>
        <source>Highscore </source>
        <translation>Рекорд</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="190"/>
        <source>Next</source>
        <translation>Следующий</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="26"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="35"/>
        <source>Difficulty</source>
        <translation>Сложность</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="38"/>
        <source>Very Hard</source>
        <translation>Очень тяжелый</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="40"/>
        <source>Hard</source>
        <translation>Тяжелый</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="42"/>
        <source>Normal</source>
        <translation>Нормальный</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="44"/>
        <source>Easy</source>
        <translation>Легкий</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="46"/>
        <source>Very Easy</source>
        <translation>Очень легкий</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="58"/>
        <source>Squares</source>
        <translation>Площади</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="58"/>
        <source>Dots</source>
        <translation>Точки</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="59"/>
        <source>Changes the shape of the blocks</source>
        <translation>Изменить форму блоков</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="75"/>
        <source>Ghost Enabled</source>
        <translation>Призрак включен</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="75"/>
        <source>Ghost Disabled</source>
        <translation>Призрак выключен</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="76"/>
        <source>Display a hint of where the block will fall</source>
        <translation>Показать область падения</translation>
    </message>
</context>
</TS>
