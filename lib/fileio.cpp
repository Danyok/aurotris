#include "../lib/fileio.h"
#include <QDir>
#include <QStringList>
#include <QStandardPaths>

//Constructor
FileIO::FileIO(QObject *parent) :
    QObject(parent)
{}



//Your custom function
void FileIO::write(const QString &filename, const QString &inputText) {
//    QDir dir("/usr/bin/io.github.danyok96/aurotris");
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris");
    if (!dir.exists())
        dir.mkpath(".");
//    QDir::setCurrent("/usr/bin/io.github.danyok96/aurotris");
    QDir::setCurrent(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris");
    QFile file( filename );
    QTextStream(stdout) << "Write Path: " << QDir::currentPath() << endl;
    if ( file.open(QIODevice::ReadWrite) ) {
        QTextStream stream( &file );
        stream << inputText << endl;
    }
    return;
}

QString FileIO::read(const QString &filename) {
//    QDir dir("/usr/bin/io.github.danyok96/aurotris");
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris");
    if (!dir.exists())
        dir.mkpath(".");
//    QDir::setCurrent("/usr/bin/io.github.danyok96/aurotris");
    QDir::setCurrent(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris");
    QString outputText;
    QFile file( filename );
    QTextStream(stdout) << "Read Path: " << QDir::currentPath() << endl;
    if ( file.exists()) {
        if ( file.open(QIODevice::ReadWrite) ) {
            QTextStream stream( &file );
            stream >> outputText;
        }
    }
    else {
        outputText = '0';
    }
    QTextStream(stdout) << filename << ": " << outputText << endl;
    return outputText;
}

void FileIO::save(const QString &slot, const QString &property, const QStringList &input) {
//    QDir dir("/usr/bin/io.github.danyok96/aurotris/Slot"+slot);
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris/Slot"+slot);
    if (!dir.exists())
        dir.mkpath(".");
//    QDir::setCurrent("/usr/bin/io.github.danyok96/aurotris/Slot"+slot);
    QDir::setCurrent(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris/Slot"+slot);
    QTextStream(stdout) << "Save Path: " << QDir::currentPath() << endl;
    QFile file( property );
    if ( file.open(QIODevice::ReadWrite) ) {
        QTextStream stream( &file );
        for (int i = 0; i < input.length(); i++) {
            stream << input.at(i) << endl;
        }
    }
    return;
}

QStringList FileIO::load(const QString &slot, const QString &property) {
//    QDir dir("/usr/bin/io.github.danyok96/aurotris/Slot"+slot);
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris/Slot"+slot);
    if (!dir.exists())
        dir.mkpath(".");
//    QDir::setCurrent("/usr/bin/io.github.danyok96/aurotris/Slot"+slot);
    QDir::setCurrent(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris/Slot"+slot);
    QTextStream(stdout) << "Load Path: " << QDir::currentPath() << endl;
    QFile file( property );
    QStringList loading;
    if ( file.open(QIODevice::ReadWrite) ) {
        QTextStream stream( &file );
        for ( int i = 0; i < 204; i++) {
            loading << stream.readLine();
        }
    }
    file.close();
    return loading;
}

void FileIO::save(const QString &slot, const QStringList &input) {
//    QDir dir("/usr/bin/io.github.danyok96/aurotris/Slot"+slot);
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris/Slot"+slot);
    if (!dir.exists())
        dir.mkpath(".");
//    QDir::setCurrent("/usr/bin/io.github.danyok96/aurotris/Slot"+slot);
    QDir::setCurrent(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris/Slot"+slot);
    QTextStream(stdout) << "Save Path: " << QDir::currentPath() << endl;
    QFile file( "values" );
    if ( file.open(QIODevice::ReadWrite) ) {
        QTextStream stream( &file );
        for (int i = 0; i < input.length(); i++) {
            stream << input.at(i) << endl;
        }
    }
    return;
}

QStringList FileIO::load(const QString &slot) {
//    QDir dir("/usr/bin/io.github.danyok96/aurotris/Slot"+slot);
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris/Slot"+slot);
    if (!dir.exists())
        dir.mkpath(".");
//    QDir::setCurrent("/usr/bin/io.github.danyok96/aurotris/Slot"+slot);
    QDir::setCurrent(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/io.github.danyok96/aurotris/Slot"+slot);
    QTextStream(stdout) << "Load Path: " << QDir::currentPath() << endl;
    QFile file( "values" );
    QStringList loading;
    if ( file.open(QIODevice::ReadWrite) ) {
        QTextStream stream( &file );
        for ( int i = 0; i < 9; i++) {
            loading << stream.readLine();
        }
    }
    file.close();
    return loading;
}
